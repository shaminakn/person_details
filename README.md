Project Description:

In this “person_details” will gives the age of given person.
The program is writing an action client server.
Installing ROS Kinetic on python 2.7:
● ROS Kinetic ONLY supports Wily (Ubuntu 15.10), Xenial (Ubuntu 16.04)
and Jessie (Debian 8) for debian packages.
http://wiki.ros.org/kinetic/Installation/Ubuntu
● Installing pip/setuptools with Linux Package Managers:
If you’re using a Python that was downloaded from python.org, then this
section does not apply. See the Requirements for Installing Packages section
instead.
sudo apt­get install pip
Verify the Pip Installation:
pip ­V

Examining the action client server:

Here we are deals with how to write an action client server node in
python.

Creating a catkin Package:
cd ~/catkin_ws/src
catkin_create_pkg person_details actionlib message_generation roscpp 
rospy std_msgs actionlib_msgs
Building a catkin workspace and sourcing the setup file:
$ cd ~/catkin_ws
$ catkin_make
Writing an action client server program  (Python):
http://wiki.ros.org/actionlib/Tutorials

%28python%29
follow this link to create the publisher and subscriber node.
License:
Subdirectories contained within the third_party directory each contain relevant
licenses for the code and software within those subdirectories.
The file TENSORFLOW_LICENSE applies to the binaries distributed in the releases.
The file LICENSE applies to other files in this repository. I want to stress that a
majority of the lines of code found in the guide of this repository was created by others.
If any of those original authors want more prominent attribution, please contact me
and we can figure out how to make it acceptable.