#! /usr/bin/env python
from __future__ import print_function
import rospy
import sys

import actionlib
# Brings in the messages used by the speech action, including the
# goal message and the result message.
import person_details.msg

def Person_client(y):
    # Creates the SimpleActionClient, passing the type of the action
    client = actionlib.SimpleActionClient('Name',person_details.msg.PersonAction)
    
    # Waits until the action server has started up and started
    # listening for goals.
    client.wait_for_server()
 
    # Creates a goal to send to the action server.
    goal = person_details.msg.PersonGoal(namz=y)

    
    #client.send_feedback(feedback)
    client.send_goal(goal)
    

    # Waits for the server to finish performing the action.
    client.wait_for_result()

    # Prints out the result of executing the action
    return client.get_result()   

if __name__ == '__main__':
    while 1:
      try:
        y=raw_input("Enter the name of a person  ")
        print(y)
        # Initializes a rospy node so that the SimpleActionClient can
        # publish and subscribe over ROS.
        rospy.init_node('Person_client')
        result =Person_client(y)
        if result.age==0:
           print("no matching name found")
        else:
           print("name:",y)
           print("Age:",result.age)
        
      except rospy.ROSInterruptException:
        print("program interrupted before completion", file=sys.stderr)

     
